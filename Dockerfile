FROM maven:3.5.2-jdk-8-alpine AS build
COPY pom.xml /tmp/
COPY /src /tmp/src/
WORKDIR /tmp/
RUN mvn package

FROM tomcat:9.0-jre8-alpine
#COPY --from=build /tmp/target/*.war $CATALINA_HOME/webapps/demo1webapp.war
COPY --from=build /tmp/target/*.war $CATALINA_HOME/webapps/ROOT.war
EXPOSE 8080
CMD ["catalina.sh", "run"]